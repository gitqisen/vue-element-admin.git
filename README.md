# vue-element-admin

>此版本为[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)修复版，并保持长期更新


安装教程
---

```shell
git clone https://gitee.com/gitqisen/vue-element-admin.git

npm install
```
>若安装失败请尝试切换npm源再安装
```shell
npm install --registry=https://registry.npm.taobao.org
```

---
